<?php
include './db_config.php';
session_start();
if (isset($_POST['sub'])) {
    $u = $_POST['user'];
    $p = $_POST['pass'];
    $s = "select * from reg where username='$u' and password= '$p'";
    $qu = mysqli_query($mysqli, $s);
    if (mysqli_num_rows($qu) > 0) {
        $f = mysqli_fetch_assoc($qu);
        $_SESSION['id'] = $f['id'];
        header('location:./index.php');
    } else {
        $_SESSION['error'] = "username or password does not exist";
        //echo 'username or password does not exist';
    }

}
?>
<html>

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- References: https://github.com/fancyapps/fancyBox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        form {
            border: 3px solid #f1f1f1;
        }

        input[type=text], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        button {
            background-color: #04AA6D;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            opacity: 0.8;
        }

        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }

        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
        }

        img.avatar {
            width: 40%;
            border-radius: 50%;
        }

        .container {
            padding: 16px;
        }

        span.psw {
            float: right;
            padding-top: 16px;
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }

            .cancelbtn {
                width: 100%;
            }
        }
    </style>
</head>
<body>
<h2>Login Form</h2>
<?php if (isset($_SESSION['error']) && !empty($_SESSION['error'])) { ?>
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            <li><?php echo $_SESSION['error']; ?></li>
        </ul>
    </div>
    <?php unset($_SESSION['error']);
    session_destroy();
} ?>
<form enctype="multipart/form-data" method="post">
    <div class="container">
        <label for="uname"><b>Username</b></label>
        <input type="text" placeholder="Enter Username" name="user" required>

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="pass" required>

        <button type="submit" name="sub">Login</button>
    </div>
</form>

<!--<form method="POST" enctype="multipart/form-data">-->
<!--    <table>-->
<!---->
<!--        <tr>-->
<!--            <td>-->
<!--                Username-->
<!--                <input type="text" name="user">-->
<!--            </td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>-->
<!--                password-->
<!--                <input type="password" name="pass">-->
<!--            </td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>-->
<!--                <input type="submit" name="sub" value="submit">-->
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
</body>
</html>
