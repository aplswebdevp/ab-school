<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
require('./db_config.php');


$sql = "SELECT * FROM image_gallery";
$images = $mysqli->query($sql);
$rows = [];
while ($row = mysqli_fetch_array($images)) {
    array_push($rows, $row['image']);
}
$halved = array_chunk($rows, ceil(count($rows) / 2));

$arr['list1'] = $halved[0];
$arr['list2'] = $halved[1];

echo json_encode($arr);
die;
